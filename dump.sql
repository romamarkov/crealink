-- --------------------------------------------------------
-- Хост:                         localhost
-- Версия сервера:               5.5.40-0ubuntu0.14.04.1 - (Ubuntu)
-- ОС Сервера:                   debian-linux-gnu
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица clink.currency
DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(10) NOT NULL DEFAULT '' COMMENT 'technical name',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'title',
  `rate` decimal(15,5) NOT NULL COMMENT 'rate',
  `updatedAt` datetime NOT NULL COMMENT 'updated at',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы clink.currency: ~4 rows (приблизительно)
DELETE FROM `currency`;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` (`id`, `name`, `title`, `rate`, `updatedAt`) VALUES
	(1, 'USD', 'Доллар США', 1.00000, '2015-01-23 09:50:51'),
	(2, 'EUR', 'Евро', 0.87970, '2015-01-23 09:50:51'),
	(3, 'BYR', 'Белорусский рубль', 15172.50000, '2015-01-23 09:50:51'),
	(4, 'UAH', 'Украинская гривна', 15.81750, '2015-01-23 09:50:51');
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
