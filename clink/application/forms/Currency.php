<?php

/**
 * Form curency
 * 
 * @author markov
 */
class Application_Form_Currency extends Zend_Form
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setMethod('post');
    }
    
    /**
     * Set params
     * 
     * @param mixed $params params
     */
    public function setParams($params)
    {
        $values = [];
        foreach ($params['currencies'] as $item) {
            $values[$item->getProperties()->get('id')] = $item->getProperties()->get('name');
        }
        $data = [
            'firstCurrency'    => $values,
            'secondCurrency'   => $values
        ];
        foreach($data as $key => $items) {
            $select = new Zend_Form_Element_Select($key);
            $select->setAttribs(['options' => $items]);
            $this->addElements([$select]);
        }
    }
}