<?php

/**
 * Currency mapper
 * 
 * @author markov
 */
class Application_Model_CurrencyMapper extends Extended_Model_CacheMapper
{
    protected $_dbTableName = 'Application_Model_DbTable_Currency';
    
    protected $_modelName = 'Application_Model_Currency';
}

