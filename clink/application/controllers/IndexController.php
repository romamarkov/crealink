<?php

/**
 * Index controller
 * 
 * @author markov
 */
class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $serviceCurrencyRate = Extended_Class_App::getServiceCurrencyRate();
        $serviceCurrencyRate->updateRates();
        $currencies = Extended_Class_App::getServiceCurrency()->getCurrencies();
        $form = Extended_Class_App::newInstanceFormCurrency();
        $form->setParams([
            'currencies'    => $currencies
        ]);
        $this->view->form = $form;
    }
    
    /**
     * update rates
     */
    public function updateRatesAction()
    {
        $serviceCurrencyRate = Extended_Class_App::getServiceCurrencyRate();
        $serviceCurrencyRate->updateRates();
    }
}

