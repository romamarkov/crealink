<?php

/**
 * Currency controller
 * 
 * @author markov
 */
class CurrencyController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    /**
     * Pair rate
     */
    public function rateAction()
    {
        $firstCurrencyId = $this->_getParam('firstCurrencyId');
        $secondCurrencyId = $this->_getParam('secondCurrencyId');
        $rate = Extended_Class_App::getServiceCurrencyRate()
            ->getRate($firstCurrencyId, $secondCurrencyId);
        echo Zend_Json::encode([
            'rate'  => $rate
        ]);	
		$this->getHelper('Layout')
            ->disableLayout();
        $this->getHelper('ViewRenderer')
            ->setNoRender();
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json');
    }
}

