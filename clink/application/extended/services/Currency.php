<?php

/**
 * Service currency
 *
 * @author markov
 */
class Extended_Service_Currency extends Extended_Service_AbstractService
{
    /**
     * Update rates
     */
    public function getCurrencies()
    {
        $currencyMapper = new Application_Model_CurrencyMapper();
        $currencies = $currencyMapper->fetchAll();
        return $currencies;
    }
}
