<?php

/**
 * Service currency rate
 *
 * @author markov
 */
class Extended_Service_CurrencyRate extends Extended_Service_AbstractService
{
    /**
     * Update rates
     */
    public function updateRates()
    {
        $currencyMapper = Extended_Class_App::getCurrencyMapper();
        $currencies = $currencyMapper->fetchAll();
        $currencyRateSource = Extended_Class_App::getDataSourceFactory()->make(
            'currencyRateSourceSimple', 
            [
                'provider'  => 'currencyRateProviderExchangerates',
                'options'   => []
            ]
        );
        $currencyNames = [];
        foreach ($currencies as $currency) {
            $currencyNames[] = $currency->getProperties()->get('name');
        }
        $rates = $currencyRateSource->execute('getRates', [
            $currencyNames
        ]);
        foreach ($currencies as $currency) {
            $name = $currency->getProperties()->get('name');
            if (!isset($rates[$name])) {
                continue;
            }
            $currency->getProperties()
                ->set('rate', $rates[$name])
                ->set('updatedAt', date('Y-m-d h:i:s'));
            $currencyMapper->save($currency);
        }
    }
    
    /**
     * Rate pair
     * 
     * @param integer $firstCurrencyId first currency
     * @param integer $secondCurrencyId second currency
     * @return number
     */
    public function getRate($firstCurrencyId, $secondCurrencyId)
    {
        $currencyMapper = Extended_Class_App::getCurrencyMapper();
        $firstCurrency = $currencyMapper->findByKey($firstCurrencyId);
        if (!$firstCurrency) {
            return false;
        }
        $secondCurrency = $currencyMapper->findByKey($secondCurrencyId);
        if (!$secondCurrency) {
            return false;
        }
        return $firstCurrency->getProperties()->get('rate') / 
            $secondCurrency->getProperties()->get('rate');
    }
}
