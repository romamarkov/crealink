<?php

/**
 * Currency rate provider
 *
 * @author markov
 */
abstract class Extended_Class_CurrencyRateProvider extends Extended_Class_DataProvider
{
    /**
     * @param array $currencies currency names
     * @return array
     */
    abstract protected function _getRates($currencies);
    
    /**
     * @param array $currency currency name
     * @return number
     */
    abstract protected function _getRate($currency);
}
