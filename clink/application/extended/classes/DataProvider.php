<?php

/**
 * Abstract data provider
 *
 * @author markov
 */
abstract class Extended_Class_DataProvider
{
    /**
     * Options
     * 
     * @var array
     */
    protected $_options;
    
    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->_options = $options;
    }
    
    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }
    
    /**
     * @param string $query
     * @param array $options
     * @return mixed
     */
    public function execute($query, $options)
    {
        $commandName = '_' . $query;
        return call_user_func_array([$this, $commandName], $options);
    }
}
