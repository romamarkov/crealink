<?php

/**
 * Cache manager
 *
 * @author markov
 */
class Extended_Class_CacheManager
{
    /**
     * @var Extended_Cache_Backend_Redis
     */
    protected $_redisCache;
    
    public function __construct()
    {
        $this->_redisCache = Zend_Cache::factory(
            new Zend_Cache_Core([
                'lifetime' => 600,
                'automatic_serialization' => false,
            ]),
            new Extended_Cache_Backend_Redis([
                'servers' => [
                    [
                        'host' => '127.0.0.1',
                        'port' => 6379,
                        'dbindex' => 1,
                    ]
                ]
            ])
        );
    }
    
    /**
     * Add to cache
     * 
     * @param mixed $data
     * @param array $keys
     * @param array $tags
     */
    public function set($data, $keys, $tags = [])
    {
        $key = md5(serialize($keys));
        $serializedData = serialize($data);
        $this->_redisCache->save($serializedData, $key, $tags);
    }
    
    /**
     * Get to cache
     * 
     * @param array $keys
     * @return mixed
     */
    public function get($keys)
    {
        $key = md5(serialize($keys));
        $serializedData = $this->_redisCache->load($key);
        if (!$serializedData) {
            return null;
        }
        $data = unserialize($serializedData);
        return $data;
    }
    
    /**
     * Clean cache
     */
    public function clean()
    {
        $this->_redisCache->clean();
    }
}
