<?php

/**
 * App
 * 
 * @author markov
 */
class Extended_Class_App
{
    /**
     * @return Extended_Service_CurrencyRate
     */
    public static function getServiceCurrencyRate()
    {
        return Extended_Class_ServiceLocator::getService('serviceCurrencyRate');
    }
    
    /**
     * @return Application_Form_Currency
     */
    public static function newInstanceFormCurrency()
    {
        return Extended_Class_ServiceLocator::newInstance('formCurrency');
    }
    
    /**
     * @return Application_Model_CurrencyMapper
     */
    public static function getCurrencyMapper()
    {
        return Extended_Class_ServiceLocator::getService('currencyMapper');
    }
    
    /**
     * @return Extended_Service_Currency
     */
    public static function getServiceCurrency()
    {
        return Extended_Class_ServiceLocator::getService('serviceCurrency');
    }
    
    /**
     * @return Extended_Class_DataSourceFactory
     */
    public static function getDataSourceFactory()
    {
        return Extended_Class_ServiceLocator::getService('dataSourceFactory');
    }
    
    /**
     * @return Extended_Class_CacheManager
     */
    public static function getCacheManager()
    {
        return Extended_Class_ServiceLocator::getService('cacheManager');
    }
}
