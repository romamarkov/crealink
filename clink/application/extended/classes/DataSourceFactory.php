<?php

/**
 * Data source factory
 * 
 * @author markov
 */
class Extended_Class_DataSourceFactory
{
    /**
     * @return Extended_Class_DataSource
     */
    public function make($sourceName, $config)
    {
        $source = Extended_Class_ServiceLocator::newInstance($sourceName);
        $source->setConfig($config);
        return $source;
    }
}
