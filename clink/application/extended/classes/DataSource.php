<?php

/**
 * Abstract data source
 *
 * @author markov
 */
abstract class Extended_Class_DataSource
{
    /**
     * Provider name
     * 
     * @var string
     */
    protected $_providerName;
    
    /**
     * Source options
     * 
     * @var array
     */
    protected $_options;
    
    /**
     * @var Extended_Class_DataProvider
     */
    protected $_provider;
    
    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->_providerName = $config['provider'];
        $this->_options = $config['options'];
    }
    
    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }
    
    /**
     * @return Extended_Class_DataProvider
     */
    public function getProvider()
    {
        if (!$this->_provider) {
            $this->_provider = Extended_Class_ServiceLocator::newInstance(
                $this->_providerName, $this->_options
            );
        }
        return $this->_provider;
    }
    
    /**
     * @return mixed
     */
    public function execute($query, $params)
    {
        return $this->getProvider()->execute($query, $params);
    }
}
