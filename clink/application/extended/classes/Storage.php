<?php

/**
 * Storage
 *
 * @author markov
 */
class Extended_Class_Storage
{
    /**
     * Storage
     * 
     * @var array
     */
    protected $storage = [];
    
    /**
     * Set value
     * 
     * @param string $name name
     * @param mixed $value value
     * @return $this
     */
    public function set($name, $value)
    {
        $this->storage[$name] = $value;
        return $this;
    }
    
    /**
     * Set arrays
     * 
     * @param array $row name
     * @return $this
     */
    public function setArray($row)
    {
        foreach ($row as $key => $item) {
            $this->set($key, $item);
        }
        return $this;
    }
    
    /**
     * Get value
     * 
     * @param string $name name
     * @return mixed 
     */
    public function get($name)
    {
        return isset($this->storage[$name]) ? $this->storage[$name] : null; 
    }
    
    /**
     * Get value
     * 
     * @param string $name name
     * @return array 
     */
    public function getAll()
    {
        return $this->storage; 
    }
}
