<?php

/**
 * Provider www.getexchangerates.com
 *
 * @author markov
 */
class Extended_Class_CurrencyRateProviderExchangerates extends Extended_Class_CurrencyRateProvider
{
    /**
     * @inheritdoc
     */
    protected function _getRates($currencies)
    {
        $rates = $this->_getData();
        $result = [];
        foreach ($currencies as $currency) {
            if (!isset($rates[$currency])) {
                continue;
            }
            $result[$currency] = $rates[$currency];
        }
        return $result;
    }
    
    /**
     * @inheritdoc
     */
    protected function _getRate($currency)
    {
        $result = $this->_getRates([$currency]);
        return isset($result[$currency]) ? $result[$currency] : null;
    }
    
    /**
     * @return array
     */
    protected function _getData()
    {
        $json = file_get_contents('http://www.getexchangerates.com/api/latest.json');
        $rates = json_decode($json, true);
        return $rates[0];
    }
}
