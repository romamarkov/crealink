<?php

/**
 * Currency rate provider simple (for example other provider)
 *
 * @author markov
 */
class Extended_Class_CurrencyRateProviderSimple extends Extended_Class_CurrencyRateProvider
{
    /**
     * @inheritdoc
     */
    protected function _getRates($currencies)
    {
        return [];
    }
    
    /**
     * @inheritdoc
     */
    protected function _getRate($currency)
    {
        return 1;
    }
}
