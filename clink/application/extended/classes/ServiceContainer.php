<?php

/**
 * Service container
 *
 * @author markov
 */
class Extended_Class_ServiceContainer
{   
    protected $storage = [];
    
    /**
     * @param string $name name service
     * @return mixed
     */
    public function get($name)
    {
        return isset($this->storage[$name]) ? $this->storage[$name] : null;
    }
    
    /**
     * @param string $name name service
     * @param mixed $service service
     * @return mixed
     */
    public function set($name, $service)
    {
        $this->storage[$name] = $service;
    }
}
