<?php

/**
 * ServiceLocator
 *
 * @author markov
 */
class Extended_Class_ServiceLocator
{
    /**
     * @var Extended_Class_ServiceContainer
     */
    protected static $_serviceContainer;
    
    /**
     * @param string $name name service
     * @return mixed
     */
    public static function getService($name)
    {
        $service = self::_getServiceContainer()->get($name);
        if (!$service) {
            $class = self::getClassName($name);
            $service = new $class;
            self::_getServiceContainer()->set($name, $service);
            return $service;
        }
        return $service;
    }
    
    /**
     * @return array
     */
    protected static function _getServicesConfig()
    {
        return require APPLICATION_PATH . '/configs/services.php';
    }
    
    /**
     * @param string $serviceName
     * @return string
     */
    public static function getClassName($serviceName)
    {
        $servicesConfig = self::_getServicesConfig();
        return isset($servicesConfig[$serviceName]) ? $servicesConfig[$serviceName] : null;
    }
    
    /**
     * @return Extended_Class_ServiceContainer
     */
    protected static function _getServiceContainer()
    {
        if (!self::$_serviceContainer) {
            self::$_serviceContainer = new Extended_Class_ServiceContainer();
        }
        return self::$_serviceContainer;
    }
    
    /**
     * Instance new object
     *
     * @param string $serviceName
     * @return mixed
     */
    public static function newInstance($serviceName)
    {
        $className = self::getClassName($serviceName);
        return new $className;
    }
}
