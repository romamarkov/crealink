<?php

/**
 * Abstract model mapper
 * 
 * @author markov
 */
abstract class Extended_Model_AbstractMapper
{
    /**
     * Table name
     * 
     * @var string 
     */
    protected $_dbTableName;
    
    /**
     * Model name
     * 
     * @var string 
     */
    protected $_modelName;
    
    /**
     * @var Zend_Db_Table_Abstract
     */
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable($this->_dbTableName);
        }
        return $this->_dbTable;
    }
    
    /**
     * Find by key
     * 
     * @param integer $id key
     * @return Extended_Model_AbstractModel
     */
    public function findByKey($id)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $entry = new $this->_modelName;
        $entry->getProperties()->setArray($row->toArray());
        return $entry;
    }
    
    /**
     * 
     * @return Extended_Model_AbstractModel <array>
     */
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries = [];
        foreach ($resultSet as $row) {
            $entry = new $this->_modelName;
            $entry->getProperties()->setArray($row->toArray());
            $entries[] = $entry;
        }
        return $entries;
    }
    
    /**
     * Save model
     * 
     * @param Extended_Model_AbstractModel $model
     */
    public function save($model)
    {
        $data = $model->getProperties()->getAll();
        if (!isset($data['id']) || $data['id'] === null) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $data['id']));
        }
    }

}

