<?php

/**
 * Abstract model
 * 
 * @author markov
 */
abstract class Extended_Model_AbstractModel
{
    /**
     * @var Extended_Storage
     */
    protected $properties = null;
    
    /**
     * @return Extended_Class_Storage
     */
    public function getProperties()
    {
        if (!$this->properties) {
            $this->properties = new Extended_Class_Storage();
        }
        return $this->properties;
    }
}

