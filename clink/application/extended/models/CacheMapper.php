<?php

/**
 * Abstract model mapper cached
 * 
 * @author markov
 */
abstract class Extended_Model_CacheMapper extends Extended_Model_AbstractMapper
{
    /**
     * @inheritdoc
     */
    public function fetchAll()
    {
        $key = ['fetchAll', $this->_modelName];
        $cache = Extended_Class_App::getCacheManager()->get($key);
        if ($cache) {
            return $cache;
        }
        $result = parent::fetchAll();
        Extended_Class_App::getCacheManager()->set($result, $key);
        return $result;
    }
    
    /**
     * @inheritdoc
     */
    public function findByKey($id)
    {
        $key = ['findByKey', $this->_modelName, $id];
        $cache = Extended_Class_App::getCacheManager()->get($key);
        if ($cache) {
            return $cache;
        }
        $result = parent::findByKey($id);
        Extended_Class_App::getCacheManager()->set($result, $key);
        return $result;
    }
}

