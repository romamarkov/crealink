<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
    }
    
    /**
     * Добавляем ресурсы в автолоадер
     * 
     */
    protected function _initAutoloader()
    {
        $resourceLoader = new Zend_Loader_Autoloader_Resource([
            'basePath'  => APPLICATION_PATH . '/extended',
            'namespace' => 'Extended',
        ]);
        $resourceLoader->addResourceType('Model', '/models', '_Model');
        $resourceLoader->addResourceType('Class', '/classes', '_Class');
        $resourceLoader->addResourceType('Service', '/services', '_Service');
        
        $resourceLoaderVendors = new Zend_Loader_Autoloader_Resource([
            'basePath'  => APPLICATION_PATH . '/vendors/Extended',
            'namespace' => 'Extended',
        ]);
        $resourceLoaderVendors->addResourceType('Cache', '/Cache', '_Cache');
    }
}

