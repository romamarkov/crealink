/**
 * @author markov
 */
var ControllerIndex = {
    
    /**
     * id first currency
     * 
     * @returns {integer}
     */
    getFirstCurrencyId: function() {
        return $('#firstCurrency').val();
    },
    
    /**
     * id second currency
     * 
     * @returns {integer}
     */
    getSecondCurrencyId: function() {
        return $('#secondCurrency').val();
    },
    
    /**
     * Initialize after jquery on ready
     */
    init: function() {
        var self = this;
        $(function() {
            self.initialize();
        });
    },
    
    /**
     * initialize
     */
    initialize: function() {
        var self = this;
        $('#firstCurrency, #secondCurrency').change(function() {
            self.updateRate();
        });
        self.updateRate();
    },
    
    /**
     * update rate
     */
    updateRate: function() {
        var params = {
            firstCurrencyId: this.getFirstCurrencyId(),
            secondCurrencyId: this.getSecondCurrencyId()
        };
        this.getRate(params, function(result) {
            $('.rate').html(result.rate);
        });
    },
    
    /**
     * Rate
     * 
     * @param {integer} firstCurrencyId
     * @param {integer} secondCurrencyId
     */
    getRate: function(params, callback) 
    {   
        var self = this;
        $.ajax({
            type: "POST",
            data: (params),
            url: "/currency/rate/",
            cache: false,
            success: function(result){
                callback(result);
            }
        });
    }
};